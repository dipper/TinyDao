package org.jeecgframework.tinydao.util;

import java.math.BigDecimal;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class BigDecimalConverter implements Converter{

	@Override
	public Object convert(Class type, Object value) {
		if(value == null){
			return null;
		}
		if(value instanceof String){
			String temp = (String) value;
			if(temp.trim().length()==0){
				return null;
			}else{
				return new BigDecimal(temp);
			}
		}else{
			throw new ConversionException("传入value值不是字符串");
			
		}
	 
	}

}
