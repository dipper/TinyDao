package org.jeecgframework.tinydao.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;

import org.apache.log4j.Logger;

/** 
* @ClassName: TinyDaoUtil 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author zx zx2009428@163.com
* @date 2015年3月22日 上午12:30:40 
*  
*/
public class TinyDaoUtil {
	private static final Logger logger = Logger.getLogger(TinyDaoUtil.class); 
	
	/**
	 * 数据库类型
	 */
	public static final String DATABASE_TYPE_MYSQL = "mysql";
	public static final String DATABASE_TYPE_POSTGER = "postgresql";
	public static final String DATABASE_TYPE_ORACLE = "oracle";
	public static final String DATABASE_TYPE_SQLSERVER = "sqlServer";
 	
	
	/**
	 * 分页SQL
	 */
	
	public static final String MYSQL_SQL = "select * from ( {0}) sel_tab00 limit {1},{2}";
	public static final String POSTGRE_SQL = "select * from ( {0}) sel_tab00 limit {2} offset {1}";
	public static final String ORACLE_SQL = "select * from (select row_.*,rownum rownum_ from ({0})) row_ where rownum<= {1}) where rownum_>{2}";
	public static final String SQLSERVER_SQL = "select * from (select row_number() over(order by tempColumn) tempRowNumber, * from (select top {1} tempColumn = 0,{0}) t) tt where tempRowNumber > {2}";
	
 public static void main(String[] args) {
	System.out.println(getFirstSmall("ABsssCss"));
	System.out.println(getFirstSmall("A"));
	System.out.println(getFirstSmall("AVB"));
}
	/** 
	* @Title: getFirstSmall 
	* @Description: 返回首字母变成小写的字符串 
	* @param @param name
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws 
	*/
	public static String getFirstSmall(String name){
		name = name.trim();
		if(name.length()>=2){
			return name.substring(0,1).toLowerCase()+name.substring(1);
		}else{
			return name.toLowerCase();
		}
	}
	
	/** 
	* @Title: isAbstract 
	* @Description: 判断方法是否是抽象方法
	* @param @param method
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws 
	*/
	public static boolean isAbstract(Method method) {
		int mod = method.getModifiers();
		return Modifier.isAbstract(mod);
	}
	/** 
	* @Title: createPaageSql 
	* @Description: 根据数据库类型封装sql
	* @param @param dbType
	* @param @param executeSql
	* @param @param page
	* @param @param rows
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws 
	*/
	public static String createPageSql(String jdbcType, String executeSql,
			int page, int rows) {
		int beginNum = (page-1)*rows;
		String[] sqlParam = new String[3];
		sqlParam[0]  = executeSql;
		sqlParam[1] = beginNum+"";
		sqlParam[2] = rows+"";
		if(jdbcType==null||"".equals(jdbcType)){
			throw new RuntimeException("org.jeecgframework.minidao.aop.TinyDaoHandler(数据库类型:dbType)没有设置,请检查配置文件");
		}
		if(jdbcType.indexOf(DATABASE_TYPE_MYSQL)!=-1){
			executeSql = MessageFormat.format(MYSQL_SQL, sqlParam);
		}else if(jdbcType.indexOf(DATABASE_TYPE_POSTGER)!=-1){
			executeSql = MessageFormat.format(POSTGRE_SQL, sqlParam);
		}else{
			int beginIndex = (page - 1) *rows;
			int endIndex = beginIndex + rows;
			sqlParam[1] = Integer.toString(endIndex);
			sqlParam[2] = Integer.toString(beginIndex);
			if(jdbcType.indexOf(DATABASE_TYPE_ORACLE)!=-1){
				executeSql = MessageFormat.format(ORACLE_SQL, sqlParam);
			}else if(jdbcType.indexOf(DATABASE_TYPE_SQLSERVER)!=-1){
				executeSql = MessageFormat.format(SQLSERVER_SQL, sqlParam);
			}
	}
		
		return executeSql;
	}
	/** 
	* @Title: isWrapClass 
	* @Description: 判断类是否是基础包装类类型
	* @param @param returnType
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws 
	*/
	public static boolean isWrapClass(Class<?> returnType) {
		 try {
			return ((Class)returnType.getField("TYPE").get(null)).isPrimitive();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
