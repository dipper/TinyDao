package org.jeecgframework.tinydao.util;

/** 
* @ClassName: CamelCaseUtils 
* @Description: 驼峰规则格式化
* @author zx zx2009428@163.com
* @date 2015年3月22日 上午1:52:18 
*  
*/
public class CamelCaseUtils {

	private static final char SEPARATOR = '_';
	public static void main(String[] args) {
		System.out.println(toUnderlineName("AjdlfdASFkdkdlf"));
		System.out.println(toCamelCase("Aj_dlfdAS_PFkdkdlf"));
		System.out.println(toCapitalizeCamelCase("Aj_dlfdAS_PFkdkdlf"));
	}
	/** 
	* @Title: toUnderlineName 
	* @Description: 小写+下划线
	* @param @param s
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws 
	*/
	public static String toUnderlineName(String s){
		if(s == null){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		boolean upperCase = false;
		for(int i = 0;i< s.length();i++){
			char c = s.charAt(i);
			boolean nextUpperCase = true;
			if(i<(s.length()-1)){
				nextUpperCase = Character.isUpperCase(s.charAt(i+1));
			}
			
			if((i>=0)&&Character.isUpperCase(c)){
				//当前位置不是大写下一个是大写的情况添加下划线
				if(!upperCase||!nextUpperCase){
					if(i>0)sb.append(SEPARATOR);
				}
				upperCase = true;
			}else {
				upperCase = false;
			}
			sb.append(Character.toLowerCase(c));
		}
		
		return sb.toString();
	}
	
	/** 
	* @Title: toCamelCase 
	* @Description: _格式化成大写驼峰格式
	* @param @param s
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws 
	*/
	public static String toCamelCase(String s){
		if(s == null){
			return null;
		}
		s = s.toLowerCase();
		StringBuilder sb = new StringBuilder(s.length());
		
		boolean uppperCase = false;
		for(int i=0;i<s.length();i++){
			char c = s.charAt(i);
			
			if(c ==SEPARATOR){
				uppperCase = true;
			}else if(uppperCase){
				sb.append(Character.toUpperCase(c));
				uppperCase = false;
			}else{
				sb.append(c);
			}
		}
		
		return sb.toString();
	}
	
	public static String toCapitalizeCamelCase(String s){
		if(s== null){
			return null;
		}
		s = toCamelCase(s);
		return s.substring(0,1).toUpperCase()+s.substring(1);
	}
	}