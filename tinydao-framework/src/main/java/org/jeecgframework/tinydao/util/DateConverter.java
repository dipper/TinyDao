package org.jeecgframework.tinydao.util;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang.time.DateUtils;

public class DateConverter implements Converter{

	String[] parsePatterns = new String[]{"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss.S"};
	
	
	@Override
	public Object convert(Class type, Object value) {
		if(value==null){
			return null;
		}
		if(value instanceof String){
			String tmp = (String)value;
			if(tmp.trim().length()==0){
				return null;
			}else{
				try{
					return DateUtils.parseDateStrictly(tmp, parsePatterns);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}else {
			throw new ConversionException("不是字符串");
		}
		return null;
	}

}
