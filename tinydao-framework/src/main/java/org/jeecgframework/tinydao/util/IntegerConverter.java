package org.jeecgframework.tinydao.util;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class IntegerConverter implements Converter {

	@Override
	public Object convert(Class type, Object value) {
		if(value==null){
			return null;
		}
		if(value instanceof String){
			String temp= (String) value;
			if(temp.trim().length()==0){
				return null;
			}else {
				return Integer.valueOf(temp);
			}
		}else {
			throw new ConversionException("不是字符串");
		}
	}

}
