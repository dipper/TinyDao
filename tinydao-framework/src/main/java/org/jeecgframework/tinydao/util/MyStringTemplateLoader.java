package org.jeecgframework.tinydao.util;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import freemarker.cache.TemplateLoader;

public class MyStringTemplateLoader implements TemplateLoader{

	private static final String DEFAULT_TEMPLATE_KEY = "_default_temp_key";
	
	private Map templates = new HashMap();
	
	
	public MyStringTemplateLoader(String defalutTemplate) {
		if(StringUtils.isNotEmpty(defalutTemplate)){
			templates.put(DEFAULT_TEMPLATE_KEY, defalutTemplate);
		}
	}
	
	public void addTemplate(String name,String template){
		if(name ==null || template == null || name.equals("")|| template.equals("")){
			return ;
		}
		if(!templates.containsKey(name)){
			templates.put(name, template);
		}
	}

	@Override
	public void closeTemplateSource(Object arg0) throws IOException {
		 
		
	}

	@Override
	public Object findTemplateSource(String name) throws IOException {
		if(StringUtils.isBlank(name)){
			name = DEFAULT_TEMPLATE_KEY;
		}
		return templates.get(name);
	}

	@Override
	public long getLastModified(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Reader getReader(Object arg0, String arg1) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
