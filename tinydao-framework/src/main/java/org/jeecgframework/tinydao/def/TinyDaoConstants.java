package org.jeecgframework.tinydao.def;

/** 
* @ClassName: TinyDaoConstants 
* @Description: 常量文件 
* @author zx zx2009428@163.com
* @date 2015年3月21日 下午9:40:45 
*  
*/
public class TinyDaoConstants {
	/**
	 * 接口方法定义规则
	 * 添加：insert add create
	 * 修改 update modify store
	 * 删除 delete remove
	 * 检索 以上单词之外
	 */
	public static final String INF_METHOD_ACTIVE = "insert,add,create,update,modify,store,delete,remove";
	public static final String INF_METHOD_BATCH = "batch";
	
	/**
	 * 方法有且只有一个参数
	 * 用户未使用@Arguments 标签
	 * 模板中引用参数默认为dto
	 */
	public static final String SQL_FTL_DTO = "dto";
	
	
	public static final String METHOD_SAVE_BY_HIBER = "saveByhiber";
	public static final String METHOD_GET_BY_ID_HIBER = "getByIdHiber";
	public static final String METHOD_GET_BY_ENTITY_HIBER = "getByEntityHiber";
	public static final String METHOD_UPDATE_BY_HIBER = "updateByHiber";
	public static final String METHOD_DELETE_BY_HIBER = "deleteByHiber";
	public static final String METHOD_LIST_BY_HIBER = "listByHiber";
	public static final String METHOD_DELETE_BY_ID_HIBER = "deleteByIdHiber";
	
	
}
