package org.jeecgframework.tinydao.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

public interface IGenericBaseCommonDao {
	
	public <T> void save(T entity);
	public <T> void saveOrUpdate(T entity);
	public <T> void delete(T entitie);
	public <T> T get(T entitie,final Serializable id);
	public <T> List<T> loadAll(T entitie);
	public <T> T get(T entitie);
	public Session getSession();
	public <T> T findUniqueByProperty(Class<T> entityClass,String protertyName,Object value);
	/**
	 * 
	* @Title: deleteEntityById 
	* @Description: 根据主键删除指定的实体
	* @param @param entityName
	* @param @param id    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public <T> void deleteEntityById(Class entityName,Serializable id);
}
