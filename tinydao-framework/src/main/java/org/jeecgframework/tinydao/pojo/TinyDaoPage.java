package org.jeecgframework.tinydao.pojo;

/** 
* @ClassName: TinyDaoPage 
* @Description: 自动分页设置 
* @author zx zx2009428@163.com
* @date 2015年3月21日 下午9:30:33 
*  
*/
public class TinyDaoPage {
	private int page;
	private int rows;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	
}
