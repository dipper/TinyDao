package org.jeecgframework.tinydao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * 规则：
 * 1.注解标签参数必须和方法参数保持一致
 * 2.注解标签参数的参数数目不能大约方法参数的数目
 * 3.只有在注解标签参数标注的参数，才会传递到SQL模板里
 * 4.如果方法参数只有一个，如果用户不设置注释标签参数，则默认参数名为miniDto
* @ClassName: Arguments 
* @Description: 记录SQL模板参数名
* @author zx zx2009428@163.com
* @date 2015年3月21日 下午10:36:07 
*  
*/
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Arguments {
	String[] value() default{};
}
