package org.jeecgframework.tinydao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
* @ClassName: ResultType 
* @Description: 定义返回的List中的具体类型，便于返回值类型确认，
* 如果没有或者是java.util.Map 则为java.util.Map 否则为对应实体类全名
* @author zx zx2009428@163.com
* @date 2015年3月22日 上午12:51:49 
*  
*/
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ResultType {

	String[] value() default{};
}
