package org.jeecgframework.tinydao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
//@Inherited：允许子类继承父类的注解。
//@Retention(RetentionPolicy.RUNTIME)表示注解的信息被保留在class文件(字节码文件)中当程序编译时，会被虚拟机保留在运行时，
//@Target(ElementType.TYPE) 表示这个注解用来修饰类
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TinyDao {

}
