package org.jeecgframework.tinydao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
* @ClassName: Sql 
* @Description: SQL注解 记录用户自定义Sql模板
* @author zx zx2009428@163.com
* @date 2015年3月21日 下午10:57:15 
*  
*/
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Sql {
	
	String value();
	String dbms() default "";

}
