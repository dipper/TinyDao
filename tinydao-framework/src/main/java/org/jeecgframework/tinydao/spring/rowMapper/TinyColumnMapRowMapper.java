package org.jeecgframework.tinydao.spring.rowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;

import org.jeecgframework.tinydao.spring.map.TinyDaoLinkedMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;

/** 
* @ClassName: TinyColumnMapRowMapper 
* @Description: 使用小写的key 作为map的关键字
* @author zx zx2009428@163.com
* @date 2015年3月22日 上午1:31:43 
*  
*/
public class TinyColumnMapRowMapper implements RowMapper<Map<String,Object>> {

	@Override
	public Map<String,Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
		 ResultSetMetaData rsmd = rs.getMetaData();
		 int columnCount = rsmd.getColumnCount();
		 Map<String,Object> mapOfColValues = createColumnMap(columnCount);
		 for(int i =1;i<= columnCount;i++){
			 String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
			 Object obj = getColumnValue(rs,i);
			 mapOfColValues.put(key, obj);
		 }
		return mapOfColValues;
	}

	private Object getColumnValue(ResultSet rs, int i) throws SQLException {
		return JdbcUtils.getResultSetValue(rs, i);
	}

	private String getColumnKey(String lookupColumnName) {
		return lookupColumnName;
	}

	private Map<String, Object> createColumnMap(int columnCount) {
		
		return new TinyDaoLinkedMap(columnCount);
	}

}
