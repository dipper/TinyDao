package org.jeecgframework.tinydao.spring.map;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/** 
* @ClassName: TinyDaoLinkedMap 
* @Description: 提供默认小写作为Key的Map
* @author zx zx2009428@163.com
* @date 2015年3月22日 上午1:31:22 
*  
*/
public class TinyDaoLinkedMap extends LinkedHashMap<String, Object>{

	private static final long serialVersionUID = 1L;
	private final Locale locale;
	public TinyDaoLinkedMap() {
		this(((Locale)(null)));
	}
	public TinyDaoLinkedMap(Locale locale) {
		this.locale = locale==null?Locale.getDefault():locale;
	}
	
	public TinyDaoLinkedMap(int initialCapacity) {
		this(initialCapacity,null);
	}
	
	public TinyDaoLinkedMap(int initialCapacity,Locale locale) {
		super(initialCapacity);
		this.locale = locale == null?Locale.getDefault():locale;
	}

	public Object put(String key,Object value){
		return super.put(convertKey(key), value);
	}
	
	public void putAll(Map map){
		if(map.isEmpty()){
			return ;
		}
		java.util.Map.Entry entry;
		for(Iterator iterator = map.entrySet().iterator();iterator.hasNext();put(
				convertKey((String)entry.getKey()),entry.getValue()))
			entry = (java.util.Map.Entry)iterator.next();
	}
	
	public boolean containsKey(Object key){
		return (key instanceof String) && super.containsKey(convertKey((String)key));
	}
	public Object get(Object key){
		if(key instanceof String)
			return super.get(convertKey((String)key));
		else
			return null;
	}
	public Object remove(Object key){
		if(key instanceof String)
			return super.remove(convertKey((String)key));
		else
				return null;
	}
	public void clear(){
		super.clear();
	}
	protected String convertKey(String key){
		return key.toLowerCase(locale);
	}
	
 
	 

	
}
