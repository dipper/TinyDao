package org.jeecgframework.tinydao.spring.rowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.collections.map.LinkedMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;

public class TinyColumnOriginalMapRowMapper implements RowMapper<Map<String,Object>> {
	
	

	public TinyColumnOriginalMapRowMapper() {
		super();
	}

	@Override
	public Map<String, Object> mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		Map<String,Object> mapOfValues = createCoumnMap(columnCount);
		for(int i=1;i<=columnCount;i++){
			String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
			Object obj  = getColumnValue(rs,i);
			mapOfValues.put(key, obj);
		}
		return mapOfValues;
	}

	private Object getColumnValue(ResultSet rs, int i) throws SQLException {
		return JdbcUtils.getResultSetValue(rs, i);
	}

	private String getColumnKey(String lookupColumnName) {
		return lookupColumnName;
	}

	private Map<String, Object> createCoumnMap(int columnCount) {
		return new LinkedMap(columnCount);
	}

}
